/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "RFDCDRC.hh"
#include <cmath> // pow
#include <stdexcept> // runtime_error

#define API_LOG_ONLY(func) \
  _LOG_DEBUG("rfdc %s", #func);

#define API_NO_RET(func, ...) \
  func(__VA_ARGS__);

#define API(func, ...) \
  API_LOG_ONLY(func) \
  if (func(__VA_ARGS__) != XRFDC_SUCCESS) { \
    throw std::runtime_error(std::string(#func)+" failure"); \
  }

#define API_NO_TYPE_RD(func, pxrfdc, tl, bl, val) \
  if (func(pxrfdc, tl, bl, val) != XRFDC_SUCCESS) { \
    throw std::runtime_error(std::string(#func)+" failure"); \
  } \
  _LOG_DEBUG("rfdc %s %i %i %u", #func, tl, bl, *val);

#define API_NO_TYPE_WR(func, pxrfdc, tl, bl, val) \
  if (func(pxrfdc, tl, bl, val) != XRFDC_SUCCESS) { \
    throw std::runtime_error(std::string(#func)+" failure"); \
  } \
  _LOG_DEBUG("rfdc %s %i %i %u", #func, tl, bl, val);

#define API_TYPE_WR(func, pxrfdc, ty, tl, bl, val) \
  if (func(pxrfdc, ty, tl, bl, val) != XRFDC_SUCCESS) { \
    throw std::runtime_error(std::string(#func)+" failure"); \
  } \
  _LOG_DEBUG("rfdc %s %i %i %i %u", #func, ty, tl, bl, val);

#define API_TYPE_AND_TILE_ONLY_WR(func, pxrfdc, ty, tl) \
  if (func(pxrfdc, ty, tl) != XRFDC_SUCCESS) { \
    throw std::runtime_error(std::string(#func)+" failure"); \
  } \
  _LOG_DEBUG("rfdc %s %i %i", #func, ty, tl);

#define API_TYPE_WR(func, pxrfdc, ty, tl, bl, val) \
  if (func(pxrfdc, ty, tl, bl, val) != XRFDC_SUCCESS) { \
    throw std::runtime_error(std::string(#func)+" failure"); \
  } \
  _LOG_DEBUG("rfdc %s %i %i %i %u", #func, ty, tl, bl, val);

#define API_MIX(func, pxrfdc, ty, tl, bl, pset) \
  if (func(pxrfdc, ty, tl, bl, pset) != XRFDC_SUCCESS) { \
    throw std::runtime_error(std::string(#func)+" failure"); \
  } \
  _LOG_DEBUG("rfdc %s %i %i %i %f %f %i %i %i %i %i", #func, ty, tl, \
      bl, (pset)->Freq, (pset)->PhaseOffset, (pset)->EventSource, \
      (pset)->CoarseMixFreq, (pset)->MixerMode, (pset)->FineMixerScale, \
      (pset)->MixerType);

#define API_DSA(func, pxrfdc, tl, bl, pset) \
  if (func(pxrfdc, tl, bl, pset) != XRFDC_SUCCESS) { \
    throw std::runtime_error(std::string(#func)+" failure"); \
  } \
  _LOG_DEBUG("rfdc %s %u %u %u %f", #func, tl, bl, \
      (pset)->DisableRTS, (pset)->Attenuation);

static RFDCCallBack* g_p_rfdc;

uint8_t get_uchar_prop(unsigned long offset,
    unsigned long prop_off) {
  return g_p_rfdc->get_uchar_prop(offset, prop_off);
}

uint16_t get_ushort_prop(unsigned long offset,
    unsigned long prop_off) {
  return g_p_rfdc->get_ushort_prop(offset, prop_off);
}

uint32_t get_ulong_prop(unsigned long offset,
    unsigned long prop_off) {
  return g_p_rfdc->get_ulong_prop(offset, prop_off);
}

uint64_t get_ulonglong_prop(unsigned long offset,
    unsigned long prop_off) {
  return g_p_rfdc->get_ulonglong_prop(offset, prop_off);
}

void set_uchar_prop(unsigned long offset, unsigned long prop_off, uint8_t val) {
  g_p_rfdc->set_uchar_prop(offset, prop_off, val);
}

void set_ushort_prop(unsigned long offset, unsigned long prop_off,
    uint16_t val) {
  g_p_rfdc->set_ushort_prop(offset, prop_off, val);
}

void set_ulong_prop(unsigned long offset, unsigned long prop_off,
    uint32_t val) {
  g_p_rfdc->set_ulong_prop(offset, prop_off, val);
}

void set_ulonglong_prop(unsigned long offset, unsigned long prop_off,
    uint64_t val) {
  g_p_rfdc->set_ulonglong_prop(offset, prop_off, val);
}

RFDCZU4XDRDRC::RFDCZU4XDRDRC(RFDCCallBack &rfdc) : DRC(), m_rfdc(rfdc),
    m_enable_inverse_sinc_fir(false) {
  memset(&m_xrfdc_config, 0, sizeof(m_xrfdc_config));
  // must "connect" this object's callback (reg interface) to the global
  // pointer before initialize()
  g_p_rfdc = &m_rfdc;
  m_description.assign("rfdc");
}

void
RFDCZU4XDRDRC::dump_regs() {
  for (auto it = m_rf_port_infos.begin(); it != m_rf_port_infos.end(); ++it) {
    API_NO_RET(XRFdc_DumpRegs, &m_xrfdc, it->second.type, it->second.tile)
  }
}

bool
RFDCZU4XDRDRC::get_rx(const std::string& rf_port_name) {
  return m_rf_port_infos.at(rf_port_name).type == XRFDC_ADC_TILE;
}

double
RFDCZU4XDRDRC::get_tuning_freq_MHz(const std::string& rf_port_name) {
  return access_radio_param(true, rf_port_name, 0, 0);
}

double
RFDCZU4XDRDRC::get_bandwidth_3dB_MHz(const std::string& rf_port_name) {
  // https://docs.xilinx.com/r/en-US/ds926-zynq-ultrascale-plus-rfsoc/Integrated-RF-DAC-Block
  // 
  return access_radio_param(true, rf_port_name, 0, 1);
}

double
RFDCZU4XDRDRC::get_sampling_rate_Msps(const std::string& rf_port_name) {
  return access_radio_param(true, rf_port_name, 0, 2);
}

bool
RFDCZU4XDRDRC::get_samples_are_complex(const std::string& /*rf_port_name*/) {
  return true;
}

std::string
RFDCZU4XDRDRC::get_gain_mode(const std::string& /*rf_port_name*/) {
  ///@TODO explore AGC functionality
  return "manual";
}

double
RFDCZU4XDRDRC::get_gain_dB(const std::string& rf_port_name) {
  return access_radio_param(true, rf_port_name, 0, 3);
}

OperatingPlan
RFDCZU4XDRDRC::get_operating_plan_for_tuning_freq_MHz(
    const ConfigurationChannel& /*chan*/, const std::string& rf_port_name,
    double val, double tolerance) {
  OperatingPlan ret;
  ret.val_to_set_to = val;
  const double nco_nvals = std::pow(2., 48.);
  if (tolerance < get_sampling_rate_Msps(rf_port_name)/2./nco_nvals) {
    ret.error.assign("tolerance not achievable by rfsoc nco");
  }
  return ret;
}

OperatingPlan
RFDCZU4XDRDRC::get_operating_plan_for_bandwidth_3dB_MHz(
    const ConfigurationChannel& chan, const std::string& rf_port_name,
    double /*val*/, double /*tolerance*/) {
  return get_operating_plan_for_fs_bw(chan, rf_port_name, false);
}

OperatingPlan
RFDCZU4XDRDRC::get_operating_plan_for_sampling_rate_Msps(
    const ConfigurationChannel& chan, const std::string& rf_port_name,
    double /*val*/, double /*tolerance*/) {
  return get_operating_plan_for_fs_bw(chan, rf_port_name, true);
}

OperatingPlan
RFDCZU4XDRDRC::get_operating_plan_for_gain_dB(
    const ConfigurationChannel& /*chan*/, const std::string& rf_port_name,
    double val, double tolerance) {
  OperatingPlan ret;
  const auto& tol = tolerance;
  if (this->m_rf_port_infos.at(rf_port_name).type == XRFDC_ADC_TILE) {
    ret = get_operating_plan_continuous_limits(val, -27., 0., tol);
    if (ret.error.empty()) {
      const double clamp_low = std::floor(val);
      const double clamp_hi = std::ceil(val);
      ret = get_operating_plan_discrete_limits(val, clamp_low, clamp_hi, tol);
    }
  }
  else {
#if 0
    const double fc = get_tuning_freq_MHz(rf_port_name);
    const RFDCZU4XDRDRC::vop_t vop = calc_vop(fc);
    double min = calc_vop_gain_dB(fc, vop.output_current_min_uamps);
    double max = calc_vop_gain_dB(fc, vop.output_current_max_uamps);
    ret = get_operating_plan_continuous_limits(val, min, max, tol);
    if (ret.error.empty()) {
      const double desired = ((double) calc_vop_current_in_uamps(fc, ret.val_to_set_to)) - vop.set_point;
      const double tmp = desired/vop.current_step_size_ua;
      const double ua_low = (std::floor(tmp)*vop.current_step_size_ua)+vop.set_point;
      const double ua_hi = (std::ceil(tmp)*vop.current_step_size_ua)+vop.set_point;
      const double clamp_low = calc_vop_gain_dB(fc, ua_low);
      const double clamp_hi = calc_vop_gain_dB(fc, ua_hi);
      ret = get_operating_plan_discrete_limits(ret.val_to_set_to, clamp_low, clamp_hi, tol);
      _LOG_DEBUG("preparing gain with adjusted value of %f db", ret.val_to_set_to);
      _LOG_DEBUG("preparing gain with current  value of %u uamps", calc_vop_current_in_uamps(fc, ret.val_to_set_to));
    }
#endif
    ret = get_operating_plan_discrete_val(val, 0., tol);
  }
  return ret;
}

/* @brief good example of parameters whose simultaneous operation are
 *        co-dependent
 * @param[in] chan contains both fs and bw settings to consider
 * @param[in] fs   if true, get plan for samp rate, otherwise get plan for
 *                 bandwidth */
OperatingPlan
RFDCZU4XDRDRC::get_operating_plan_for_fs_bw(const ConfigurationChannel& chan,
    const std::string& rf_port_name, const bool fs) {
  OperatingPlan ret;
  ret.error.assign("init");
  const double fs_val = chan.sampling_rate_Msps;
  const double fs_tol = chan.tolerance_sampling_rate_Msps;
  const double bw_val = chan.bandwidth_3dB_MHz;
  const double bw_tol = chan.tolerance_bandwidth_3dB_MHz;
  const auto& tile = m_rf_port_infos.at(rf_port_name).tile;
  double max_gigsps;
  if (m_rf_port_infos.at(rf_port_name).type == XRFDC_ADC_TILE) {
    max_gigsps = m_xrfdc_config[0].ADCTile_Config[tile].SamplingRate;
  }
  else {
    max_gigsps = m_xrfdc_config[0].DACTile_Config[tile].SamplingRate;
  }
  const double max_megsps = max_gigsps * 1000.;
  const double fs_max = max_megsps;
  const bool rx = m_rf_port_infos.at(rf_port_name).type == XRFDC_ADC_TILE;
  const double bw_max = rx ? 4000. : 6000.;
  const double val = fs ? fs_val : bw_val;
  const double tol = fs ? fs_tol : bw_tol;
  double max = fs ? fs_max : bw_max;
  const auto interp_decim_vals = get_possible_interp_decim_vals(rf_port_name);
  for (auto it = interp_decim_vals.begin(); it != interp_decim_vals.end(); ++it) {
    if (!ret.error.empty()) {
      // first check if dependent parameter operation is possible for the
      // interp/decim val being considered...
      if (fs) {
        if (!get_operating_plan_discrete_val(bw_val, bw_max/(*it), bw_tol).error.empty()) {
          continue;
        }
      }
      /*else {
        if (!get_operating_plan_discrete_val(fs_val, fs_max/(*it), fs_tol).error.empty()) {
          continue;
        }
      }*/
      // ... and if so, formulate a plan for the param in question (fs or bw)
      // for the same interp/decim val
      ret = get_operating_plan_discrete_val(val, max/(*it), tol);
    }
  }
  if (!ret.error.empty()) {
    ret.error.assign("not achievable with possible interp/decim values");
  }
  return ret;
}

rfdc_ip_version_t
RFDCZU4XDRDRC::get_fpga_rfdc_ip_version() {
  rfdc_ip_version_t ret;
  uint32_t reg_0 = get_ulong_prop(0, 0);
  ret.major = (reg_0 & 0xff000000) >> 24;
  ret.minor = (reg_0 & 0x00ff0000) >> 16;
  return ret;
}

u32
RFDCZU4XDRDRC::get_xrfdc_interp_decim_factor(double dfact) {
  u32 ret = XRFDC_INTERP_DECIM_1X;
  if ((dfact > 1.5) and (dfact < 2.5)) {
    ret = XRFDC_INTERP_DECIM_2X;
  }
  else if (dfact < 3.5) {
    ret = XRFDC_INTERP_DECIM_3X;
  }
  else if (dfact < 4.5) {
    ret = XRFDC_INTERP_DECIM_4X;
  }
  else if (dfact < 5.5) {
    ret = XRFDC_INTERP_DECIM_5X;
  }
  else if (dfact < 7.) {
    ret = XRFDC_INTERP_DECIM_6X;
  }
  else if (dfact < 9.) {
    ret = XRFDC_INTERP_DECIM_8X;
  }
  else if (dfact < 11.) {
    ret = XRFDC_INTERP_DECIM_10X;
  }
  else if (dfact < 14.) {
    ret = XRFDC_INTERP_DECIM_12X;
  }
  else if (dfact < 18.) {
    ret = XRFDC_INTERP_DECIM_16X;
  }
  else if (dfact < 22.) {
    ret = XRFDC_INTERP_DECIM_20X;
  }
  else if (dfact < 32.) {
    ret = XRFDC_INTERP_DECIM_24X;
  }
  else {
    ret = XRFDC_INTERP_DECIM_40X;
  }
  return ret;
}

std::vector<double>
RFDCZU4XDRDRC::get_possible_interp_decim_vals(const std::string& /*rf_port_name*/) {
  return {1, 2, 3, 4, 5, 6, 8, 10, 12, 16, 20, 24, 40};
}

void
RFDCZU4XDRDRC::set_rx(const std::string& /*rf_port_name*/, bool /*val*/) {
}

void
RFDCZU4XDRDRC::set_tuning_freq_MHz(const std::string& rf_port_name,
    double val) {
  access_radio_param(false, rf_port_name, val, 0);
}

void
RFDCZU4XDRDRC::set_bandwidth_3dB_MHz(const std::string& rf_port_name,
    double val) {
  access_radio_param(false, rf_port_name, val, 1);
}

void
RFDCZU4XDRDRC::set_sampling_rate_Msps(const std::string& rf_port_name,
    double val) {
  access_radio_param(false, rf_port_name, val, 2);
}

void
RFDCZU4XDRDRC::set_samples_are_complex(const std::string& /*rf_port_name*/,
    bool /*val*/) {
}

void
RFDCZU4XDRDRC::set_gain_mode(const std::string& /*rf_port_name*/,
    const std::string& /*val*/) {
}

void
RFDCZU4XDRDRC::set_gain_dB(const std::string& rf_port_name,
    const double val) {
  access_radio_param(false, rf_port_name, val, 3);
}

void
RFDCZU4XDRDRC::set_app_port_num(const std::string& /*rf_port_name*/,
    uint8_t /*val*/) {
}

void
RFDCZU4XDRDRC::set_enable_inverse_sinc_fir(bool val) {
  m_enable_inverse_sinc_fir = val;
}


void
RFDCZU4XDRDRC::set_datapath_mode(u32 tile, u32 block) {
  // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_SetDataPathMode-Gen-3/DFE
  u32 mode = XRFDC_DATAPATH_MODE_DUC_0_FSDIVTWO;
  API_NO_TYPE_WR(XRFdc_SetDataPathMode, &m_xrfdc, tile, block, mode)
}

double
RFDCZU4XDRDRC::access_tuning_freq_MHz(bool read, double val,
    u32 type, u32 tile, u32 block, double fs_megsps) {
  double ret = 0.;
  // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/struct-XRFdc_Mixer_Settings
  XRFdc_Mixer_Settings settings;
  if (!read && (type == XRFDC_DAC_TILE)) {
    // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_SetNyquistZone
    u32 nz = (fmod(val, fs_megsps) < fs_megsps/2.) ? 1 : 2;
    // below line must occur before XRFdc_SetMixerSettings
    API_TYPE_WR(XRFdc_SetNyquistZone, &m_xrfdc, type, tile, block, nz)
    if (m_enable_inverse_sinc_fir) {
      u16 mode = 0;
      if (val < fs_megsps/2.) {
        mode = 1;
      }
      else if (val < fs_megsps) {
        mode = 2;
      }
      // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_SetInvSincFIR
      API_NO_TYPE_WR(XRFdc_SetInvSincFIR, &m_xrfdc, tile, block, mode)
    }
  }
  if (read) {
    // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_GetMixerSettings
    API_MIX(XRFdc_GetMixerSettings, &m_xrfdc, type, tile, block, &settings)
    ret = (type == XRFDC_ADC_TILE ? -1. : 1.) * settings.Freq;
  }
  else {
    // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_SetMixerSettings
    settings.Freq = (type == XRFDC_ADC_TILE ? -1. : 1.) * val;
    settings.PhaseOffset = 0.;
    // below line ref https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/Dynamic-Update-Events
    settings.EventSource = XRFDC_EVNT_SRC_TILE;
    settings.CoarseMixFreq = XRFDC_COARSE_MIX_BYPASS;
    if (type == XRFDC_ADC_TILE) {
      settings.MixerMode = XRFDC_MIXER_MODE_R2C;
    }
    else {
      settings.MixerMode = XRFDC_MIXER_MODE_C2C;
    }
    settings.FineMixerScale = XRFDC_MIXER_SCALE_1P0;
    settings.MixerType = XRFDC_MIXER_TYPE_FINE;
    API_MIX(XRFdc_SetMixerSettings, &m_xrfdc, type, tile, block, &settings)
    // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_UpdateEvent
    API_TYPE_WR(XRFdc_UpdateEvent, &m_xrfdc, type, tile, block, XRFDC_EVENT_MIXER)
  }
  return ret;
}

double
RFDCZU4XDRDRC::access_bandwidth_3dB_MHz(bool read, double val,
    u32 type, u32 tile, u32 block) {
  return access_fs_bw(read, 6000., val, type, tile, block);
}

double
RFDCZU4XDRDRC::access_sampling_rate_Msps(bool read, double val,
    u32 type, u32 tile, u32 block) {
  double fs_gigsps;
  if (type == XRFDC_ADC_TILE) {
    fs_gigsps = (double)m_xrfdc_config[0].ADCTile_Config[tile].SamplingRate;
  }
  else {
    fs_gigsps = (double)m_xrfdc_config[0].DACTile_Config[tile].SamplingRate;
  }
  if ((type == XRFDC_DAC_TILE) && (!read)) {
    set_datapath_mode(tile, block);
  }
  return access_fs_bw(read, fs_gigsps*1000., val, type, tile, block);
}

/// @param[in] fc center frequency for tile, ignored if not a dac tile
/// @param[in] vop_uamps ignored if not a dac tile
double
RFDCZU4XDRDRC::access_gain_dB(bool read, double val,
    u32 type, u32 tile, u32 block, double fc) {
  double ret = 0.;
  if (type == XRFDC_ADC_TILE) {
    // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/struct-XRFdc_DSA_Settings-Gen-3/DFE
    XRFdc_DSA_Settings settings;
    if (read) {
      // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_GetDSA-Gen-3/DFE
      API_DSA(XRFdc_GetDSA, &m_xrfdc, tile, block, &settings)
      ret = settings.Attenuation;
    }
    else {
      // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_SetDSA-Gen-3/DFE
      settings.DisableRTS = 1;
      settings.Attenuation = -val;
      API_DSA(XRFdc_SetDSA, &m_xrfdc, tile, block, &settings)
    }
  }
  else {
    u32 vop_uamps;
    if (read) {
      // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_GetOutputCurr
      API_NO_TYPE_RD(XRFdc_GetOutputCurr, &m_xrfdc, tile, block, &vop_uamps)
      ret = calc_vop_gain_dB(fc, vop_uamps);
    }
    else {
      // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_SetDACVOP-Gen-3/DFE
      vop_uamps = calc_vop_current_in_uamps(fc, val);
      API_NO_TYPE_WR(XRFdc_SetDACVOP, &m_xrfdc, tile, block, vop_uamps)
    }
  }
  return ret;
}

double
RFDCZU4XDRDRC::access_fs_bw(bool read, double default_val, double desired_val,
    u32 type, u32 tile, u32 block) {
  u32 factor = get_xrfdc_interp_decim_factor(default_val/desired_val);
  if (type == XRFDC_ADC_TILE) {
    if (read) {
      // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_GetDecimationFactor
      API_NO_TYPE_RD(XRFdc_GetDecimationFactor, &m_xrfdc, tile, block, &factor)
    }
    else {
      // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_SetDecimationFactor
      API_NO_TYPE_WR(XRFdc_SetDecimationFactor, &m_xrfdc, tile, block, factor)
    }
  }
  else {
    if (read) {
      // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_GetInterpolationFactor
      API_NO_TYPE_RD(XRFdc_GetInterpolationFactor, &m_xrfdc, tile, block, &factor)
    }
    else {
      // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/XRFdc_SetInterpolationFactor
      API_NO_TYPE_WR(XRFdc_SetInterpolationFactor, &m_xrfdc, tile, block, factor)
    }
  }
  return default_val/((double)factor);
}

/// @param[in] val value to be written, if read is true, otherwise ignored
/// @return 0 if read is true, otherwise return the value that was read
double
RFDCZU4XDRDRC::access_radio_param(bool read,
    const std::string& rf_port_name, double val, int param) {
  double ret;
  u32 type = m_rf_port_infos.at(rf_port_name).type;
  u32 tile = m_rf_port_infos.at(rf_port_name).tile;
  std::vector<u32>& blocks = m_rf_port_infos.at(rf_port_name).blocks;
  for (auto it = blocks.begin(); it != blocks.end(); ++it) {
    if (param == 0) {
      double fs_gigsps;
      if (type == XRFDC_ADC_TILE) {
        fs_gigsps = (double)m_xrfdc_config[0].ADCTile_Config[tile].SamplingRate;
      }
      else {
        fs_gigsps = (double)m_xrfdc_config[0].DACTile_Config[tile].SamplingRate;
      }
      ret = access_tuning_freq_MHz(read, val, type, tile, *it, fs_gigsps*1000.);
    }
    else if (param == 1) {
      ret = access_bandwidth_3dB_MHz(read, val, type, tile, *it);
    }
    else if (param == 2) {
      ret = access_sampling_rate_Msps(read, val, type, tile, *it);
    }
    else if (param == 3) {
      const double fc = get_tuning_freq_MHz(rf_port_name);
      ret = access_gain_dB(read, val, type, tile, *it, fc);
    }
  }
  return ret;
}

/// @brief checking proof of life here since the rfdc lib does not
void
RFDCZU4XDRDRC::throw_if_proof_of_life_reg_test_fails() {
  rfdc_ip_version_t version = get_fpga_rfdc_ip_version();
  const bool major_match = version.major == get_expected_ip_version_major();
  const bool minor_match = version.minor == get_expected_ip_version_minor();
  if (!(major_match && minor_match)) {
    throw std::runtime_error("proof of life version register test failed");
  }
}

/*! @brief ref https://github.com/Xilinx/embeddedsw/tree/xilinx_v2021.1/XilinxProcessorIPLib/drivers/rfdc
 *         note we do NOT call XRFdc_LookupConfig() as is usually the case with
 *         this library - this is because we are not looking up by sysfs/device,
 *         we simply "register" the opencpi control plane interface as the
 *         libmetal io interface and bypass sysfs altogether since opencpi hdl
 *         containers won't have the device tree that corresponds to the sysfs
 *         entry
 ******************************************************************************/
void
RFDCZU4XDRDRC::init_metal() {
  struct metal_init_params metal_param = METAL_INIT_DEFAULTS;
  metal_param.log_level = METAL_LOG_DEBUG;
  if (metal_init(&metal_param)) {
    throw std::runtime_error("metal_init failed");
  }
}

void
RFDCZU4XDRDRC::initialize(const std::vector<std::string>& rf_ports_rx,
    const std::vector<std::string>& rf_ports_tx) {
  // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/Resets
  // https://docs.xilinx.com/r/en-US/pg269-rf-data-converter/Power-up-Sequence
  init_clock_source_devices();
  m_rfdc.take_rfdc_axi_lite_out_of_reset();
  const size_t RFDC_GEN3_ADC_CONVERT_CALIB_TIME_MICROSEC = 63000;
  usleep(2*RFDC_GEN3_ADC_CONVERT_CALIB_TIME_MICROSEC);
  m_rfdc.take_rfdc_axi_stream_out_of_reset();
  init_metal();
  throw_if_proof_of_life_reg_test_fails();
  init_xrfdc_config();
  API(XRFdc_CfgInitialize, &m_xrfdc, m_xrfdc_config)
  log_xrfdc_status();
  for (auto it = m_rf_port_infos.begin(); it != m_rf_port_infos.end(); ++it) {
    const u32 ty = it->second.type;
    const u32 tl = it->second.tile;
    if (ty != XRFDC_ADC_TILE) {
      API_TYPE_AND_TILE_ONLY_WR(XRFdc_Shutdown, &m_xrfdc, ty, tl)
    }
  }
  DRC::initialize(rf_ports_rx, rf_ports_tx);
}

void
RFDCZU4XDRDRC::log_xrfdc_status() {
  XRFdc_IPStatus st;
  memset(&st, 0, sizeof(st));
  API(XRFdc_GetIPStatus, &m_xrfdc, &st)
  for (u32 tl = 0; tl <= 3; tl++) {
    if (st.DACTileStatus[tl].IsEnabled) {
      _LOG_DEBUG("rfdc d tl%i en %i", tl, st.DACTileStatus[tl].IsEnabled);
      _LOG_DEBUG("rfdc d tl%i st %i", tl, st.DACTileStatus[tl].TileState);
      auto& state = st.DACTileStatus[tl].PowerUpState;
      _LOG_DEBUG("rfdc d tl%i pu %i", tl, state);
      _LOG_DEBUG("rfdc d tl%i pl %i", tl, st.DACTileStatus[tl].PLLState);
    }
  }
  for (u32 tl= 0; tl <= 3; tl++) {
    if (st.ADCTileStatus[tl].IsEnabled) {
      _LOG_DEBUG("rfdc a tl%i en %i", tl, st.ADCTileStatus[tl].IsEnabled);
      _LOG_DEBUG("rfdc a tl%i st %i", tl, st.ADCTileStatus[tl].TileState);
      auto& state = st.ADCTileStatus[tl].PowerUpState;
      _LOG_DEBUG("rfdc a tl%i pu %i", tl, state);
      _LOG_DEBUG("rfdc a tl%i pl %i", tl, st.ADCTileStatus[tl].PLLState);
    }
  }
}

typename RFDCZU4XDRDRC::vop_t
RFDCZU4XDRDRC::calc_vop(const double fc) {
  RFDCZU4XDRDRC::vop_t ret;
  // min_uamps and max_uamps are the AC coupling values from 
  // 1) "Table: RF-DAC Electrical Characteristics for ZU4xDR Devices" in
  // https://docs.xilinx.com/r/en-US/ds926-zynq-ultrascale-plus-rfsoc/RF-DAC-Electrical-Characteristics
  // as well as
  const double datasheet_min_uamps = 2.25*1000.;//6.425*1e3;
  const double datasheet_max_uamps = 40.5*1000.;//32.*1e3;
  ret.current_step_size_ua = 43.75;
  // these datasheet values are not actually achievable by the API and must be adjusted
  ret.set_point = 1400.;
  ret.output_current_min_uamps = std::ceil((datasheet_min_uamps-ret.set_point)/ret.current_step_size_ua)*ret.current_step_size_ua+ret.set_point;
  ret.output_current_max_uamps = std::floor((datasheet_max_uamps-ret.set_point)/ret.current_step_size_ua)*ret.current_step_size_ua+ret.set_point;
  // 3rd-degree polynomical best fit line
  ret.variable_output_power_range_db = 3.919*pow(10, -11) * pow(fc, 3);
  ret.variable_output_power_range_db += -3.819*pow(10, -7) * pow(fc, 2);
  ret.variable_output_power_range_db += -0.000314*fc + 24.097;
  /// @todo figure out why divide by two is necessary (split over to 50 ohm?)
  ret.variable_output_power_range_db /= 2.;
  return ret;
}

u32
RFDCZU4XDRDRC::calc_vop_current_in_uamps(const double fc,
    double desired_gain_db, bool apply_offset) {
  const RFDCZU4XDRDRC::vop_t vop = calc_vop(fc);
  const double ua = vop.output_current_max_uamps - vop.output_current_min_uamps;
  // in 2.0.0 and earlier release, the gain corresponding to 19993 microamps was treated as 0 dB
  if (apply_offset) {
    const double offset_tmp = (19993.-vop.output_current_min_uamps)/ua;
    desired_gain_db += offset_tmp*vop.variable_output_power_range_db;
  }
  const double tmp = desired_gain_db/vop.variable_output_power_range_db*ua;
  return (u32)(std::round(tmp + vop.output_current_min_uamps));
}

double
RFDCZU4XDRDRC::calc_vop_gain_dB(const double fc, const double vop_uamps, bool apply_offset) {
  const RFDCZU4XDRDRC::vop_t vop = calc_vop(fc);
  double ua = vop.output_current_max_uamps - vop.output_current_min_uamps;
  const double tmp = (vop_uamps-vop.output_current_min_uamps)/ua;
  double gain_db = tmp*vop.variable_output_power_range_db;
  // in 2.0.0 and earlier release, the gain corresponding to 19993 microamps was treated as 0 dB
  if (apply_offset) {
    const double offset_tmp = (19993.-vop.output_current_min_uamps)/ua;
    gain_db -= offset_tmp*vop.variable_output_power_range_db;
  }
  return gain_db;
}
