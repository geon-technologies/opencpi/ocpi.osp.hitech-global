/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "drc-worker.hh"

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace DrcWorkerTypes;

#define _LOG_INFO(fmt, ...) DrcWorkerBase::log(8, fmt, ##__VA_ARGS__)
#define _LOG_DEBUG(fmt, ...) DrcWorkerBase::log(10, fmt, ##__VA_ARGS__)
//#include <iostream>
//#define _LOG_INFO(fmt, ...) printf("[INFO]  "); printf(fmt, ##__VA_ARGS__); printf("\n")
//#define _LOG_DEBUG(fmt, ...) printf("[DEBUG] "); printf(fmt, ##__VA_ARGS__); printf("\n")
#include "HTGDRC.hh"

/*! @brief DEPENDENCIES:
 *         HTGZRF848DRDRC                     (HITECH GLOBAL ZRF8 SUPPORT)
 *         RFDCCallBack/LMX2594ProxyCallBack  (DEVICE ACCESS) */
class DrcWorker : public DrcWorkerBase {
  struct RFDC : RFDCCallBack {
    Slaves &m_slaves;
    RFDC(Slaves &slaves) : m_slaves(slaves) {
    }
    void access_prop(uint16_t addr, unsigned long pof, uint8_t* buf, size_t sz,
        bool read) {
      if (pof == RFDC_IP_CTRL_BASE_ADDR) {
        if (read) {
          m_slaves.rfdc.getRawPropertyBytes(addr, buf, sz);
        }
        else {
          m_slaves.rfdc.setRawPropertyBytes(addr, buf, sz);
        }
      }
      else if (pof == RFDC_IP_DAC0_BASE_ADDR) {
        if (read) {
          m_slaves.rfdc_dac_config_0.getRawPropertyBytes(addr, buf, sz);
        }
        else {
          m_slaves.rfdc_dac_config_0.setRawPropertyBytes(addr, buf, sz);
        }
      }
      else if (pof == RFDC_IP_DAC1_BASE_ADDR) {
        if (read) {
          m_slaves.rfdc_dac_config_1.getRawPropertyBytes(addr, buf, sz);
        }
        else {
          m_slaves.rfdc_dac_config_1.setRawPropertyBytes(addr, buf, sz);
        }
      }
      else if (pof == RFDC_IP_DAC2_BASE_ADDR) {
        if (read) {
          m_slaves.rfdc_dac_config_2.getRawPropertyBytes(addr, buf, sz);
        }
        else {
          m_slaves.rfdc_dac_config_2.setRawPropertyBytes(addr, buf, sz);
        }
      }
      else if (pof == RFDC_IP_DAC3_BASE_ADDR) {
        if (read) {
          m_slaves.rfdc_dac_config_3.getRawPropertyBytes(addr, buf, sz);
        }
        else {
          m_slaves.rfdc_dac_config_3.setRawPropertyBytes(addr, buf, sz);
        }
      }
      else if (pof == RFDC_IP_ADC0_BASE_ADDR) {
        if (read) {
          m_slaves.rfdc_adc_config_0.getRawPropertyBytes(addr, buf, sz);
        }
        else {
          m_slaves.rfdc_adc_config_0.setRawPropertyBytes(addr, buf, sz);
        }
      }
      else if (pof == RFDC_IP_ADC1_BASE_ADDR) {
        if (read) {
          m_slaves.rfdc_adc_config_1.getRawPropertyBytes(addr, buf, sz);
        }
        else {
          m_slaves.rfdc_adc_config_1.setRawPropertyBytes(addr, buf, sz);
        }
      }
      else if (pof == RFDC_IP_ADC2_BASE_ADDR) {
        if (read) {
          m_slaves.rfdc_adc_config_2.getRawPropertyBytes(addr, buf, sz);
        }
        else {
          m_slaves.rfdc_adc_config_2.setRawPropertyBytes(addr, buf, sz);
        }
      }
      else if (pof == RFDC_IP_ADC3_BASE_ADDR) {
        if (read) {
          m_slaves.rfdc_adc_config_3.getRawPropertyBytes(addr, buf, sz);
        }
        else {
          m_slaves.rfdc_adc_config_3.setRawPropertyBytes(addr, buf, sz);
        }
      }
    }
    // of  - address byte "of"fset relative to the rfdc*hdl slave worker
    //       property space
    // pof - address byte "p"roperty "off"set, or the offset of the worker's
    //       first property within the Xilinx RFDC IP address space (this is
    //       simply used to indicate which worker is to be accessed, based on
    //       the already defined segments from io.h)
    uint8_t get_uchar_prop(unsigned long of, unsigned long pof) {
      uint8_t ret;
      access_prop((uint16_t)of, pof, (uint8_t*)(&ret), sizeof(uint8_t), true);
      return ret;
    }
    uint16_t get_ushort_prop(unsigned long of, unsigned long pof) {
      uint16_t ret;
      access_prop((uint16_t)of, pof, (uint8_t*)(&ret), sizeof(uint16_t), true);
      return ret;
    }
    uint32_t get_ulong_prop(unsigned long of, unsigned long pof) {
      uint32_t ret;
      access_prop((uint16_t)of, pof, (uint8_t*)(&ret), sizeof(uint32_t), true);
      return ret;
    }
    uint64_t get_ulonglong_prop(unsigned long of, unsigned long pof) {
      uint64_t ret;
      access_prop((uint16_t)of, pof, (uint8_t*)(&ret), sizeof(uint64_t), true);
      return ret;
    }
    void set_uchar_prop(unsigned long of, unsigned long pof, uint8_t val) {
      access_prop((uint16_t)of, pof, (uint8_t*)(&val), sizeof(uint8_t), false);
    }
    void set_ushort_prop(unsigned long of, unsigned long pof, uint16_t val) {
      access_prop((uint16_t)of, pof, (uint8_t*)(&val), sizeof(uint16_t), false);
    }
    void set_ulong_prop(unsigned long of, unsigned long pof, uint32_t val) {
      access_prop((uint16_t)of, pof, (uint8_t*)(&val), sizeof(uint32_t), false);
    }
    void set_ulonglong_prop(unsigned long of, unsigned long pof, uint64_t val) {
      access_prop((uint16_t)of, pof, (uint8_t*)(&val), sizeof(uint64_t), false);
    }
    void take_rfdc_axi_lite_out_of_reset() {
      m_slaves.rfdc.start();
    }
    void take_rfdc_axi_stream_out_of_reset() {
      m_slaves.rfdc.set_axis_resetn(true);
    }
  } m_rfdc;
  struct LMX2594Proxy : LMX2594ProxyCallBack {
    Slaves &m_slaves;
    LMX2594Proxy(Slaves &slaves) : m_slaves(slaves) {
    }
    void set_register(size_t addr, uint16_t val) {
      if (addr == 0) {
        m_slaves.lmx2594_proxy.set_register_0(val);
      }
      else if (addr == 1) {
        m_slaves.lmx2594_proxy.set_register_1(val);
      }
      else if (addr == 2) {
        m_slaves.lmx2594_proxy.set_register_2(val);
      }
      else if (addr == 3) {
        m_slaves.lmx2594_proxy.set_register_3(val);
      }
      else if (addr == 4) {
        m_slaves.lmx2594_proxy.set_register_4(val);
      }
      else if (addr == 5) {
        m_slaves.lmx2594_proxy.set_register_5(val);
      }
      else if (addr == 6) {
        m_slaves.lmx2594_proxy.set_register_6(val);
      }
      else if (addr == 7) {
        m_slaves.lmx2594_proxy.set_register_7(val);
      }
      else if (addr == 8) {
        m_slaves.lmx2594_proxy.set_register_8(val);
      }
      else if (addr == 9) {
        m_slaves.lmx2594_proxy.set_register_9(val);
      }
      else if (addr == 10) {
        m_slaves.lmx2594_proxy.set_register_10(val);
      }
      else if (addr == 11) {
        m_slaves.lmx2594_proxy.set_register_11(val);
      }
      else if (addr == 12) {
        m_slaves.lmx2594_proxy.set_register_12(val);
      }
      else if (addr == 13) {
        m_slaves.lmx2594_proxy.set_register_13(val);
      }
      else if (addr == 14) {
        m_slaves.lmx2594_proxy.set_register_14(val);
      }
      else if (addr == 15) {
        m_slaves.lmx2594_proxy.set_register_15(val);
      }
      else if (addr == 16) {
        m_slaves.lmx2594_proxy.set_register_16(val);
      }
      else if (addr == 17) {
        m_slaves.lmx2594_proxy.set_register_17(val);
      }
      else if (addr == 18) {
        m_slaves.lmx2594_proxy.set_register_18(val);
      }
      else if (addr == 19) {
        m_slaves.lmx2594_proxy.set_register_19(val);
      }
      else if (addr == 20) {
        m_slaves.lmx2594_proxy.set_register_20(val);
      }
      else if (addr == 21) {
        m_slaves.lmx2594_proxy.set_register_21(val);
      }
      else if (addr == 22) {
        m_slaves.lmx2594_proxy.set_register_22(val);
      }
      else if (addr == 23) {
        m_slaves.lmx2594_proxy.set_register_23(val);
      }
      else if (addr == 24) {
        m_slaves.lmx2594_proxy.set_register_24(val);
      }
      else if (addr == 25) {
        m_slaves.lmx2594_proxy.set_register_25(val);
      }
      else if (addr == 26) {
        m_slaves.lmx2594_proxy.set_register_26(val);
      }
      else if (addr == 27) {
        m_slaves.lmx2594_proxy.set_register_27(val);
      }
      else if (addr == 28) {
        m_slaves.lmx2594_proxy.set_register_28(val);
      }
      else if (addr == 29) {
        m_slaves.lmx2594_proxy.set_register_29(val);
      }
      else if (addr == 30) {
        m_slaves.lmx2594_proxy.set_register_30(val);
      }
      else if (addr == 31) {
        m_slaves.lmx2594_proxy.set_register_31(val);
      }
      else if (addr == 32) {
        m_slaves.lmx2594_proxy.set_register_32(val);
      }
      else if (addr == 33) {
        m_slaves.lmx2594_proxy.set_register_33(val);
      }
      else if (addr == 34) {
        m_slaves.lmx2594_proxy.set_register_34(val);
      }
      else if (addr == 35) {
        m_slaves.lmx2594_proxy.set_register_35(val);
      }
      else if (addr == 36) {
        m_slaves.lmx2594_proxy.set_register_36(val);
      }
      else if (addr == 37) {
        m_slaves.lmx2594_proxy.set_register_37(val);
      }
      else if (addr == 38) {
        m_slaves.lmx2594_proxy.set_register_38(val);
      }
      else if (addr == 39) {
        m_slaves.lmx2594_proxy.set_register_39(val);
      }
      else if (addr == 40) {
        m_slaves.lmx2594_proxy.set_register_40(val);
      }
      else if (addr == 41) {
        m_slaves.lmx2594_proxy.set_register_41(val);
      }
      else if (addr == 42) {
        m_slaves.lmx2594_proxy.set_register_42(val);
      }
      else if (addr == 43) {
        m_slaves.lmx2594_proxy.set_register_43(val);
      }
      else if (addr == 44) {
        m_slaves.lmx2594_proxy.set_register_44(val);
      }
      else if (addr == 45) {
        m_slaves.lmx2594_proxy.set_register_45(val);
      }
      else if (addr == 46) {
        m_slaves.lmx2594_proxy.set_register_46(val);
      }
      else if (addr == 47) {
        m_slaves.lmx2594_proxy.set_register_47(val);
      }
      else if (addr == 48) {
        m_slaves.lmx2594_proxy.set_register_48(val);
      }
      else if (addr == 49) {
        m_slaves.lmx2594_proxy.set_register_49(val);
      }
      else if (addr == 50) {
        m_slaves.lmx2594_proxy.set_register_50(val);
      }
      else if (addr == 51) {
        m_slaves.lmx2594_proxy.set_register_51(val);
      }
      else if (addr == 52) {
        m_slaves.lmx2594_proxy.set_register_52(val);
      }
      else if (addr == 53) {
        m_slaves.lmx2594_proxy.set_register_53(val);
      }
      else if (addr == 54) {
        m_slaves.lmx2594_proxy.set_register_54(val);
      }
      else if (addr == 55) {
        m_slaves.lmx2594_proxy.set_register_55(val);
      }
      else if (addr == 56) {
        m_slaves.lmx2594_proxy.set_register_56(val);
      }
      else if (addr == 57) {
        m_slaves.lmx2594_proxy.set_register_57(val);
      }
      else if (addr == 58) {
        m_slaves.lmx2594_proxy.set_register_58(val);
      }
      else if (addr == 59) {
        m_slaves.lmx2594_proxy.set_register_59(val);
      }
      else if (addr == 60) {
        m_slaves.lmx2594_proxy.set_register_60(val);
      }
      else if (addr == 61) {
        m_slaves.lmx2594_proxy.set_register_61(val);
      }
      else if (addr == 62) {
        m_slaves.lmx2594_proxy.set_register_62(val);
      }
      else if (addr == 63) {
        m_slaves.lmx2594_proxy.set_register_63(val);
      }
      else if (addr == 64) {
        m_slaves.lmx2594_proxy.set_register_64(val);
      }
      else if (addr == 65) {
        m_slaves.lmx2594_proxy.set_register_65(val);
      }
      else if (addr == 66) {
        m_slaves.lmx2594_proxy.set_register_66(val);
      }
      else if (addr == 67) {
        m_slaves.lmx2594_proxy.set_register_67(val);
      }
      else if (addr == 68) {
        m_slaves.lmx2594_proxy.set_register_68(val);
      }
      else if (addr == 69) {
        m_slaves.lmx2594_proxy.set_register_69(val);
      }
      else if (addr == 70) {
        m_slaves.lmx2594_proxy.set_register_70(val);
      }
      else if (addr == 71) {
        m_slaves.lmx2594_proxy.set_register_71(val);
      }
      else if (addr == 72) {
        m_slaves.lmx2594_proxy.set_register_72(val);
      }
      else if (addr == 73) {
        m_slaves.lmx2594_proxy.set_register_73(val);
      }
      else if (addr == 74) {
        m_slaves.lmx2594_proxy.set_register_74(val);
      }
      else if (addr == 75) {
        m_slaves.lmx2594_proxy.set_register_75(val);
      }
      else if (addr == 76) {
        m_slaves.lmx2594_proxy.set_register_76(val);
      }
      else if (addr == 77) {
        m_slaves.lmx2594_proxy.set_register_77(val);
      }
      else if (addr == 78) {
        m_slaves.lmx2594_proxy.set_register_78(val);
      }
      else if (addr == 79) {
        m_slaves.lmx2594_proxy.set_register_79(val);
      }
      else if (addr == 80) {
        m_slaves.lmx2594_proxy.set_register_80(val);
      }
      else if (addr == 81) {
        m_slaves.lmx2594_proxy.set_register_81(val);
      }
      else if (addr == 82) {
        m_slaves.lmx2594_proxy.set_register_82(val);
      }
      else if (addr == 83) {
        m_slaves.lmx2594_proxy.set_register_83(val);
      }
      else if (addr == 84) {
        m_slaves.lmx2594_proxy.set_register_84(val);
      }
      else if (addr == 85) {
        m_slaves.lmx2594_proxy.set_register_85(val);
      }
      else if (addr == 86) {
        m_slaves.lmx2594_proxy.set_register_86(val);
      }
      else if (addr == 87) {
        m_slaves.lmx2594_proxy.set_register_87(val);
      }
      else if (addr == 88) {
        m_slaves.lmx2594_proxy.set_register_88(val);
      }
      else if (addr == 89) {
        m_slaves.lmx2594_proxy.set_register_89(val);
      }
      else if (addr == 90) {
        m_slaves.lmx2594_proxy.set_register_90(val);
      }
      else if (addr == 91) {
        m_slaves.lmx2594_proxy.set_register_91(val);
      }
      else if (addr == 92) {
        m_slaves.lmx2594_proxy.set_register_92(val);
      }
      else if (addr == 93) {
        m_slaves.lmx2594_proxy.set_register_93(val);
      }
      else if (addr == 94) {
        m_slaves.lmx2594_proxy.set_register_94(val);
      }
      else if (addr == 95) {
        m_slaves.lmx2594_proxy.set_register_95(val);
      }
      else if (addr == 96) {
        m_slaves.lmx2594_proxy.set_register_96(val);
      }
      else if (addr == 97) {
        m_slaves.lmx2594_proxy.set_register_97(val);
      }
      else if (addr == 98) {
        m_slaves.lmx2594_proxy.set_register_98(val);
      }
      else if (addr == 99) {
        m_slaves.lmx2594_proxy.set_register_99(val);
      }
      else if (addr == 100) {
        m_slaves.lmx2594_proxy.set_register_100(val);
      }
      else if (addr == 101) {
        m_slaves.lmx2594_proxy.set_register_101(val);
      }
      else if (addr == 102) {
        m_slaves.lmx2594_proxy.set_register_102(val);
      }
      else if (addr == 103) {
        m_slaves.lmx2594_proxy.set_register_103(val);
      }
      else if (addr == 104) {
        m_slaves.lmx2594_proxy.set_register_104(val);
      }
      else if (addr == 105) {
        m_slaves.lmx2594_proxy.set_register_105(val);
      }
      else if (addr == 106) {
        m_slaves.lmx2594_proxy.set_register_106(val);
      }
    }
    void set_in_filename(const std::string& val) {
      m_slaves.lmx2594_proxy.set_in_filename(val.c_str());
    }
    void start() {
      m_slaves.lmx2594_proxy.start();
    }
  } m_lmx2594_proxy;
  HTGZRF848DRDRC m_drc;
public:
  DrcWorker() : m_rfdc(slaves), m_lmx2594_proxy(slaves),
      m_drc(m_rfdc, m_lmx2594_proxy) {
  }
  void perform_action_for_all_configurations(unsigned action) {
    const size_t configuration_length = properties().configurations.size();
    for (size_t idx = 0; idx < configuration_length; idx++) {
      const auto& status = m_drc.get_status();
      if (action == 0) {
        if (status.at(idx).state != state_t::operating) {
          m_drc.set_start(idx);
        }
      }
      else if (action == 1) {
        if (status.at(idx).state == state_t::operating) {
          m_drc.set_stop(idx);
        }
      }
      else if (action == 2) {
        m_drc.set_release(idx);
      }
      else {
        const auto &prop_conf = properties().configurations.data[idx];
        OCPI::DRC::Configuration drc_configuration;
        drc_configuration.description.assign(prop_conf.description);
        drc_configuration.recoverable = prop_conf.recoverable;
        for (uint16_t ch = 0; ch < prop_conf.channels.length; ch++) {
          const auto& prop_chan = prop_conf.channels.data[ch];
          OCPI::DRC::ConfigurationChannel drc_channel;
          drc_channel.description.assign(prop_chan.description);
          drc_channel.rx = prop_chan.rx;
          drc_channel.tuning_freq_MHz = prop_chan.tuning_freq_MHz;
          drc_channel.bandwidth_3dB_MHz = prop_chan.bandwidth_3dB_MHz;
          drc_channel.sampling_rate_Msps = prop_chan.sampling_rate_Msps;
          drc_channel.samples_are_complex = prop_chan.samples_are_complex;
          drc_channel.gain_mode.assign(prop_chan.gain_mode);
          drc_channel.gain_dB = prop_chan.gain_dB;
          double tol = prop_chan.tolerance_tuning_freq_MHz;
          drc_channel.tolerance_tuning_freq_MHz = tol;
          tol = prop_chan.tolerance_bandwidth_3dB_MHz;
          drc_channel.tolerance_bandwidth_3dB_MHz = tol;
          tol = prop_chan.tolerance_sampling_rate_Msps;
          drc_channel.tolerance_sampling_rate_Msps = tol;
          drc_channel.tolerance_gain_dB = prop_chan.tolerance_gain_dB;
          drc_channel.rf_port_name.assign(prop_chan.rf_port_name);
          drc_channel.rf_port_num = prop_chan.rf_port_num;
          drc_channel.app_port_num = prop_chan.app_port_num;
          drc_configuration.channels.emplace(ch, drc_channel);
        }
        m_drc.set_configuration(idx, drc_configuration);
      }
    }
    if (action == 3) {
      m_drc.set_configuration_length(configuration_length);
    }
  }
  RCCResult initialize() {
    // see HTG OSP 2.0.0 release Software Design .docx
    // Table "HiTech Global ZRF8-48DR ... Port Connections"
    std::string adc0, adc2, dac32, dac30;
    adc0.assign(DRC_RF_PORTS_RX.data[0]);
    adc2.assign(DRC_RF_PORTS_RX.data[1]);
    dac32.assign(DRC_RF_PORTS_TX.data[0]);
    dac30.assign(DRC_RF_PORTS_TX.data[1]);
    const std::vector<std::string> rx = {DRC_RF_PORTS_RX.data[0], DRC_RF_PORTS_RX.data[1]};
    const std::vector<std::string> tx = {DRC_RF_PORTS_TX.data[0], DRC_RF_PORTS_TX.data[1]};
    m_drc.initialize(rx, tx, adc0, adc2, dac32, dac30);
    return RCC_OK;
  }
  RCCResult stop() {
    perform_action_for_all_configurations(1);
    return RCC_OK;
  }
  RCCResult release() {
    perform_action_for_all_configurations(2);
    return RCC_OK;
  }
  // notification that configurations property has been written
  RCCResult configurations_written() {
    perform_action_for_all_configurations(3);
    return RCC_OK;
  }
  // notification that prepare property has been written
  RCCResult prepare_written() {
    m_drc.set_prepare(properties().prepare);
    return RCC_OK;
  }
  // notification that start property has been written
  RCCResult start_written() {
    m_drc.set_start(properties().start);
    return RCC_OK;
  }
  // notification that stop property has been written
  RCCResult stop_written() {
    m_drc.set_stop(properties().stop);
    return RCC_OK;
  }
  // notification that release property has been written
  RCCResult release_written() {
    m_drc.set_release(properties().release);
    return RCC_OK;
  }
  // notification that status property will be read
  RCCResult status_read() {
    OCPI::DRC::Status drc_status = m_drc.get_status();
    properties().status.resize(drc_status.size());
    for (size_t idx = 0; idx < drc_status.size(); idx++) {
      auto &prop_conf = properties().status.data[idx];
      if (drc_status[idx].state == state_t::inactive) {
        prop_conf.state = STATUS_STATE_INACTIVE;
      }
      else if (drc_status[idx].state == state_t::prepared) {
        prop_conf.state = STATUS_STATE_PREPARED;
      }
      else if (drc_status[idx].state == state_t::operating) {
        prop_conf.state = STATUS_STATE_OPERATING;
      }
      else {
        prop_conf.state = STATUS_STATE_ERROR;
      }
      const size_t nchars = drc_status[idx].error.size();
      memcpy(prop_conf.error, drc_status[idx].error.c_str(), nchars);
      prop_conf.channels.resize(drc_status[idx].channels.size());
      for (size_t ch = 0; ch < prop_conf.channels.length; ch++) {
        auto& prop_chan = prop_conf.channels.data[ch];
        const auto& drc_chan = drc_status[idx].channels[ch];
        prop_chan.tuning_freq_MHz = drc_chan.tuning_freq_MHz;
        prop_chan.bandwidth_3dB_MHz = drc_chan.bandwidth_3dB_MHz;
        prop_chan.sampling_rate_Msps = drc_chan.sampling_rate_Msps;
        prop_chan.gain_dB = drc_chan.gain_dB;
      }
    }
    return RCC_OK;
  }
  // notification that tics_pro_filename property has been written
  RCCResult tics_pro_filename_written() {
    m_drc.set_tics_pro_filename(properties().tics_pro_filename);
    return RCC_OK;
  }
  // notification that dump_regs property has been written
  RCCResult dump_regs_written() {
    if (properties().dump_regs) {
      m_drc.dump_regs();
    }
    return RCC_OK;
  }
};

DRC_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented in any case
DRC_END_INFO
