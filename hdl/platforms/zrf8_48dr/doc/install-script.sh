#!/bin/bash
set -e

PATCHES_PATH=$PWD/ocpi.osp.hitech-global/patch_files/
echo "Patches will be applied from path: $PATCHES_PATH"

echo ">>>>>>>>>> Installing OpenCPI for your host" 
git clone https://gitlab.com/geon-technologies/opencpi/opencpi.git opencpi
# the commit id 5d08604e633d6b61aa9ba89e2dd41d0613e3a2be points to the rfdc
# branch as of 06/21/24
cd opencpi
git checkout 5d08604e633d6b61aa9ba89e2dd41d0613e3a2be
git apply ${PATCHES_PATH}/06fbc336fb4de3bd5967a6853f298732bb75b55e.diff 
./scripts/install-opencpi.sh --minimal
source ./cdk/opencpi-setup.sh -r 

echo ">>>>>>>>>> Obtaining Hi-Tech Global OSP"
# below line is a copy, and not a symlink, so that that "cp relative directories ../../assets" lines below work as intended
cp -rp ../ocpi.osp.hitech-global projects/osps/ocpi.osp.hitech-global
cd projects/osps/ocpi.osp.hitech-global/
ocpidev register project 

echo ">>>>>>>>>> Moving OSP Artifacts"
cp ./hdl/platforms/zrf8_48dr/doc/gsg_artifacts/build.tcl ../../assets/hdl/assemblies/testbias/.
cp ./hdl/platforms/zrf8_48dr/doc/gsg_artifacts/testbias.Makefile.patch ../../assets/hdl/assemblies/testbias/.
cd ../../assets/hdl/assemblies/testbias/
git apply testbias.Makefile.patch 
cd ../../../../../ 

echo ">>>>>>>>>> Building core HDL pritivies for zynq_ultra" 
cd projects/core/hdl/primitives
ocpidev build --hdl-target zynq_ultra
cd ../../../../

echo ">>>>>>>>>> Cloning ocpi.comp.sdr library project, running initial builds" 
git clone https://gitlab.com/opencpi/comp/ocpi.comp.sdr.git projects/ocpi.comp.sdr
cd projects/ocpi.comp.sdr/
# the commit id 766036502e089486eef78abed5dbf50716fa142a points to the develop
# branch as of 06/19/24
git checkout 766036502e089486eef78abed5dbf50716fa142a
git apply ${PATCHES_PATH}/ocpi.comp.sdr.patch
ocpidev register project 
cd hdl/primitives 
ocpidev build --hdl-platform zrf8_48dr
cd ../../
ocpidev build
cd ../../

echo ">>>>>>>>>> Building HTG primitivies"
cd projects/osps/ocpi.osp.hitech-global/hdl/primitives
ocpidev build --hdl-target zynq_ultra
cd ../../../../../

ocpiadmin install platform zrf8_48dr --minimal
 
echo ">>>>>>>>>> Install xilinx21_1_aarch64 RCC platform" 
ocpiadmin install platform xilinx21_1_aarch64 --minimal

echo ">>>>>>>>>> Dev Environment Install Complete" 
