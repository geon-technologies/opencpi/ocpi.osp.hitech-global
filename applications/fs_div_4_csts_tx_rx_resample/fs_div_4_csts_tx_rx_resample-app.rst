.. fs_div_4_csts_tx_rx_resample application

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

..

:orphan:

.. _fs_div_4_csts_tx_rx_resample:

``fs_div_4_csts_tx_rx_resample.xml`` Application
================================================

Purpose
-------

Verifies Digital Radio Controller (DRC)-based control, and HDL worker sources and sinks, including the CSTS flush operation, for I/Q
data at a highly decimated/interpolated data rate of 90 ksps, for all RF port on the HiTech Global
ZRF8-48DR using a spectrum analyzer to verify TX and an I/Q snapshot to file for RX. Additionally,
it validates the rfdc.hdl's support for the ``flush`` opcode of the CSTS protocol, using the
``fs_div_4_generator_csts.hdl`` worker to optionally produces a ``flush`` opcode on a periodic
basis. When the ``flush`` opcode is received by the ``rfdc.hdl``, a zero-valued I/Q sample is
written to the DAC which has the affect of removing the energy on the transmitter RF.
Verification is described for TX (J3,J13) and RX (J18,J20) but the application is expected to
actually transmit/receive on all available ZRF8-48DR ports.

.. _application_diagram:

This application serves as an example for using the HiTech Global RF Data Converter and its DRC
interface with the ocpi.comp.sdr resampling workers that allow for a sampling rate different
than the builtime-fixed rate of 90 Msps for the RF Data Converter. The cic_decimator_xs.hdl and
cic_interpolator_xs.hdl workers decimate and interpolate, respectively, by a factor of 1000,
resulting in a baseband sampling rate of 90 ksps. The HiTech Global's J18 and J20 RX RF ports
are dumped to a file named j18_rx.bin and j20_rx.bin during runtime, respectively. A recommended
test setup is to connect J18 or J20 to a signal generator with a 390.975 MHz tone at -30 dBm and
observing the baseband signal with a complex sinusoid at 25 kHz and sampled at 90 ksps using the
``plot_bin.m`` octave script. The ``flush_clock_count`` property sets the number of control plane
clock cycles between issuing of a ``flush`` opcode over the CSTS protocol.

Known limitations
-----------------

* Tested Configurations:

  * ``zrf8_48dr`` built with Xilinx Vivado 2021.1 and ``xilinx21_1_aarch64``

Prerequisites
-------------

#. HiTech Global's J3 or J13 TX RF port is connected to a spectrum analyzer
#. HiTech Global's J20 RX RF port is connected to a signal generator: 390.975 MHz, -30 dBm
#. Power is applied to the HiTech Global and spectrum analyzer
#. A host system is available which has octave installed for plotting the rx output
#. The ``zrf8_48dr`` has been properly installed and deployed, and the following assets are built
   and their artifacts (.bitz file for HDL and .so for RCC) exist within a directory that exists FIRST
   within colon-separate OCPI_LIBRARY_PATH environment variable value:

   * HDL Assembly: ``fs_div_4_csts_tx_rx_resample`` (ocpi.osp.hitech_global) built for ``zrf8_48dr`` platform

      * ``fs_div_4_csts_tx_rx_resample_zrf8_48dr_cfg_rfdc_j3_j13_j18_j20_cnt.bitz``

   * RCC Worker: ``drc.rcc`` (ocpi.osp.hitech_global) built for ``xilinx21_1_aarch64`` platform

      * ``drc.so``

   * RCC Worker: ``lmx2594_proxy.rcc`` (ocpi.osp.hitech_global) built for ``xilinx21_1_aarch64`` platform

      * ``lmx2594_proxy.so``

   * RCC Worker: ``file_write.rcc`` (ocpi.core) built for ``xilinx21_1_aarch64`` platform

      * ``file_write.so``

#. The ``fs_div_4_csts_tx_rx_resample.xml`` file is in the working directory

Execution
---------

#. Boot the zrf8_48dr and setup for the desired mode (Standalone)
#. Run the application::

     ocpirun -t 10 fs_div_4_csts_tx_rx_resample.xml

Verify
------

While monitoring the HiTech Global J3 or J13 output via a spectrum analyzer, confirm that
1) a transmission occurs fc - (fs/4)/up_sample_factor) = 390.95 - (90/4)/1000 = 390.9275 MHz
2) the transmission occurs for the entire runtime duration with approximately 1 second of transmission followed by 1 second of no transmission, repeating until the application is complete.

Also confirm that running the ``plot_bin.m`` octave script, from the directory containing
``j20_rx.bin``, plots the received data complex sinusoid at +25 kHz::

     octave --persist plot_bin.m

Troubleshooting
---------------

If a runtime log occurs that indicates "lock FAILED", the drc configurations property was likely not
set according to the constrained ranges described in its worker documentation. An example log
output of a user requesting a tuning freq of 1000 MHz, which the underlying radio was not capable
of, produces the following error::

    [INFO] lock SUCCEEDED for rf_port_name: J13 for config: direction for value: 1
    [INFO] lock FAILED for rf_port_name: J13 for config: tuning_freq_MHz for value: 1000 w/ tolerance: +/- 0.01
    [INFO] for rf_port_name J13: unlocking config direction
    [INFO] rf_port_name J13 did not meet requirements
    Exiting for exception: Code 0x17, level 0, error: 'Worker "drc" produced an error during the "start" control operation: config prepare request was unsuccessful, set OCPI_LOG_LEVEL to 8 (or higher) for more info'

If a runtime log occurs that indicates "error loading" with a message "undefined symbol" that refers to the DRC base class, e.g. symbol _ZN4OCPI3DRC3DRC17set_configurationEtRKNS0_13ConfigurationEb is missing, then ensure the opencpi.git repository was built with the runtime/drc/src/DRC.cc source file which defines the DRC base class, and that the ocpirun is being built from said reposity and is first in the runtime PATH ('objdump -x ocpirun' can be used to grep to confirm for the missing symbol)

* Known Issues

  * The cic_decimator_xs.hdl workers numerically overflows without producing an error. This is
    mitigated by limiting the composite amplitude/gain of the:

    #. signal sent to the RX RF ports by the signal generator
    #. cic_decimator_xs.hdl scale_output_value
