# v3.0.0
 - modified rfdc.hdl worker port protocols to user complex_short_timed_samples (CSTS) protocol instead of iqstream
 - modified ocpi.comp.sdr project dependency to be based on its latest develop branch instead of 2.4.5
 - added rfdc.hdl support for complex_short_timed_samples (CSTS) flush operation to turn of RF transmission
 - added apps for verifying rfdc.hdl complex_short_timed_samples (CSTS) support
 - fixed rfdc.hdl worker bug where RX path clock crossing occured where it shouldn't
 - removed ocpidev2 script (now exists within gitlab.com/geon-technologies/opencpi/opencpi.git alongside ocpidev script)
 - removed apps which were based on outdated rfdc.hdl iqstream support
 - updated Getting Started Guide

# v2.1.0
 - added ocpidev2 script RCC (--rcc-platform ) support
 - added ocpidev2 script unit test ("ocpidev2 unittest" command) support
 - improved app documentation formatting for uniformity
 - improved drc.rcc worker to use rfdc DSA API call to extend gain range and allow smaller step sizes
 - improved drc.rcc worker to use rfdc mixer NCO API call instead of app worker phs_inc
 - improved drc.rcc worker to no longer transmit on stop (or release) drc state transitions
 - fixed rfdc hdl primitive bug where build failed when using --hdl-platform
 - fixed zrf8_48dr.hdl platform worker bug where control plane clock rate was not 100 MHz
 - fixed fs_div_4_tx app bug where drc bandwidth was not properly specified
 - fixed drc.rcc worker bug where code assumes RF Data Converter engineering sample
 - fixed drc.rcc worker bug where error not produced for unachievable tolerances
 - fixed drc.rcc worker bug where errors on configurations property write after prepare
 - fixed various ocpidev2 script bugs
 - fixed rfdc.hdl worker bug where property writes were not being applied to RFSoC registers
